# HyperFPGA-BSP

This repository contains the board support package for the HyperFPGA nodes.

## Import the HyperFPGA into Vivado

Follow this steps to start working in **Vivado**:

1) Clone this repo.
2) Copy the ```ICTP_MLab``` folder into ```<Vivado installation directory>/Xilinx/Vivado/2023.2/data/xhub/boards/XilinxBoardStore/boards/```
3) Open **Vivado**, now when creating a new project you should see it pop at the prat selection window.

![part_selection](.img/part_selection.png)

## Missing part

1) Fork the repo
2) Create a new folder for the part under ```<vendor_name>/hyperfpga-<part number>```
3) Copy the files from any of the other implementations in ```ICTP_Mlab/hyperfpga-4ge21/1.0/1.0/```
4) Modify the files accordingly.
5) Update the picture.
6) [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
